import CryptoJS from 'crypto-js'
import { Block } from './block.js'

export class Blockchain {
    constructor() { this.chain = [] }

    initChain() {
      this.chain = [new Block(0, '0', new Date().getTime() / 1000, "OG Block Data", "9wNnNriS7Lhp72KnjRPmXsbykIpVJy07")]
    }

    replaceWholeChain(newBlocks) {
      if (newBlocks.length > this.chain.length) {
          console.log('Updating Blockchain')
          this.chain = newBlocks
          // Broadcast to all peers the latest block
      } else console.log("Blockchain doesn't need to be updated 2")
    }

    generateNewBlock(blockData) {
        const previousBlock = this.getLatestBlock()
        const nextIndex = previousBlock.index + 1
        const nextTimestamp = new Date().getTime() / 1000
        const nextHash = this.calculateHash(nextIndex, previousBlock.hash, nextTimestamp, blockData)
        return new Block(nextIndex, previousBlock.hash, nextTimestamp, blockData, nextHash)
    }

    getLatestBlock() { return this.chain[this.chain.length - 1] }

    calculateHash(index, previousHash, timestamp, data) {
        return CryptoJS.SHA256(index + previousHash + timestamp + data).toString()
    }
}