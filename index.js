import express from "express"
import http from "http"
import socketio from "socket.io"
import io_client from "socket.io-client"
import { Blockchain } from "./blockchain.js"

const app = express()
const server = http.Server(app)
const io = socketio(server)

const myPort = 3000 + Math.ceil(Math.random() * 100)
const initClient = 3000 + parseInt(process.argv[2])

let blockchain = new Blockchain()

server.listen(myPort, () => { console.log(`Listening on port ${myPort}!!`) })

io.on("connection", async (socket) => {
  socket.emit('success')
  socket.on('newPeer', theirPort => { finishAddingPeer(theirPort, socket.id) })
  socket.on('allPeers', peerList => { peerList.forEach(peerPort => addNewPeer(peerPort)) })
  socket.on('latestBlock', block => { handleLatestBlock(block, socket.id) })
  socket.on('queryAllBlocks', () => sendAllBlocks(socket.id))
  socket.on('allBlocks', (chain) => blockchain.replaceWholeChain(chain))
  socket.on('disconnect', () => removePeer(socket.id))
})

let peers = []
if (process.argv[2]) { addNewPeer(initClient) }
else blockchain.initChain()

function addNewPeer(peerPort) {
  if (getAllPeers().concat(myPort).includes(peerPort)) return
  const peer_socket = io_client(`http://localhost:${peerPort}`)
  peer_socket.on('success', () => peer_socket.emit('newPeer', myPort))
  peers.push({ theSocket: peer_socket, data: { port: peerPort } })
}

function finishAddingPeer(peerPort, socketId) {
  if (!getAllPeers().concat(myPort).includes(peerPort) || peers) {
    const peer = peers.find(peer => peer.data.port == peerPort)
    const theSocket = null
    if (peer) {
      peer.data.socketId = socketId
    } else {
      const peer_socket = io_client(`http://localhost:${peerPort}`)
      peers.push({ theSocket: peer_socket, data: { port: peerPort, socketId }})
      peer_socket.emit('newPeer', myPort)
      setTimeout(function(){ peer_socket.emit('allPeers', getAllPeers()); sendLatestBlock(peerPort) }, 1000)
    }
    listPeerPorts()
  } else if (peerPort != myPort) listPeerPorts()
}

function getAllPeers() { return peers.map(peer => peer.data.port) }
function removePeer(socketId) { peers = peers.filter(peer => socketId != peer.data.socketId); listPeerPorts() }
function listPeerPorts() {
  let str = `Connected Peer Ports: `
  peers.forEach(peer => str += ` ${peer.data.port},`)
  console.log(str)
}

//////// Blockchain Functions
function sendLatestBlockToAll() {
  peers.forEach(peer => { peer.theSocket.emit('latestBlock', blockchain.getLatestBlock()) })
}

function sendLatestBlock(peerPort) {
  const peer_socket = peers.find(p => p.data.port == peerPort).theSocket
  peer_socket.emit('latestBlock', blockchain.getLatestBlock())
}

function sendAllBlocks(socketId) {
  const peer_socket = peers.find(peer => socketId == peer.data.socketId).theSocket
  peer_socket.emit('allBlocks', blockchain.chain)
}

function queryAllBlocks(socketId) {
  const peer_socket = peers.find(peer => socketId == peer.data.socketId).theSocket
  peer_socket.emit('queryAllBlocks')
}

function handleLatestBlock(recvdBlock, socketId) {
  if (recvdBlock == null) {
    const peer = peers.find(peer => peer.data.socketId == socketId);
    sendLatestBlock(peer.data.port)
    return
  }
  if (blockchain.chain.length == 0) {
    console.log('Our Blockchain has not been iniated. Quering the whole blockchain from peer')
    queryAllBlocks(socketId); return
  }
  const currentLatestBlock = blockchain.getLatestBlock()
  if (recvdBlock.index > currentLatestBlock.index) {
    console.log('Blockchain Behind. Our latest block is: ' + currentLatestBlock.index + ' Peer has: ' + recvdBlock.index)
    if (currentLatestBlock.hash != recvdBlock.previousHash) {
      console.log("We have to query the chain from our peer"); queryAllBlocks(socketId)
    } else {
      /// Add single new block
      console.log("We can append the received block to our chain")
      blockchain.chain.push(recvdBlock)
    }
  } else console.log("Blockchain doesn't need to be updated")
}


////// http interface
app.get('/addBlock', (req, res) => {
  const newBlock = blockchain.generateNewBlock(req.query.data)
  console.log("Adding new block")
  blockchain.chain.push(newBlock)
  sendLatestBlockToAll()
  res.send(blockchain.chain)
})

app.get('/blocks', (req, res) => { res.send(blockchain.chain) })
app.get('/peers', (req, res) => { res.send(getAllPeers()) })